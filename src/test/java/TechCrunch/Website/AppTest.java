package TechCrunch.Website;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


/**
 * Unit test for simple App.
 */
public class AppTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AppTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\merve.unsal\\Desktop\\PC\\otomasyon\\chromedriver.exe");
        String URL = "https://techcrunch.com/";
        WebDriver driver = new ChromeDriver();
        driver.get(URL);

        try {
            Thread.sleep(1000);

            for(int i=1; i<=7; i++){
                String xpathAuthor = "//*[@id='tc-main-content']/div[2]/div/div/div/article[" + i + "]/header/div/div/span/span/a";
                String xpathImage = "//*[@id='tc-main-content']/div[2]/div/div/div/article["+ i +"]/footer/figure/picture/img";
         
                // AUTHOR
                if (driver.findElements(By.xpath(xpathAuthor)).size() != 0) {
                    System.out.println(i + ". news has an author.");
                }else{
                    System.out.println(i + ". news has not an author.");
                }

                // IMAGE
                if (driver.findElements(By.xpath(xpathImage)).isEmpty()) {
                    System.out.println(i + ". news has not an image.");
                }else{
                    System.out.println(i + ". news has image.");
                }         
       }
         
            	//String xpathNews = "//*[@id='tc-main-content']/div[2]/div/div/div/article[3]/div";
            	//driver.findElement(By.xpath("//*[@id='tc-main-content']/div[2]/div/div/div/article[6]/div/p")).click();
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //driver.close();
        }

    }


    public void visitSite() {

    }
}
